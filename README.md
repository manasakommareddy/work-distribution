# work-distribution

This is a work distribution service that assigns tasks to agents.
Tech Stack: Express (Node.js), Mongo DB

```
git clone to remote repository
cd work-distribution
```

# To Run from Docker container

`docker-compose up`

# To Run locally

1. MongoDB
   installtion guide: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/
   `ps -ef | grep mongod` --> to check if MongoDB is running

```
    Note: Connection url for mongodb.
    Local connection string:   url: "mongodb://localhost:27017/work-distribution"
    Docker conenction string:  url: "mongodb://mongo:27017/work-distribution"
```

2. DB client
   MongoDB Compass: https://docs.mongodb.com/compass/master/install/

3. Run node applicaiton
   `npm run server` || `node server.js` || `npm start`

# Endpoints

1.  _Endpoint that will allow creating a new task and distributing that task to an agent. This
    endpoint will accept a task object and will return that task, updated with the assigned
    agent if one was available._

        POST http://localhost:3000/create-task

2.  _Endpoint that will allow updating a task to mark it as completed._

    PUT http://localhost:3000/complete-task/:taskName

3.  _Additional Endpoints _
    a. To create new agent

    POST http://localhost:3000/create-agent

    b. *_To get all agents_*

    GET http://localhost:3000/agents

    c. *To retrieve single agent with agentId as param*

    GET http://localhost:3000/agents/:agentId

    d. *Too Update agent name with agentId*

    PUT http://localhost:3000/agents/agent-name/:agentId

    e. *Too Update agent skills with agentId*

    PUT http://localhost:3000/agents/agent-skills/:agentId

    f. *To delete agent with agentId*

    DELETE http://localhost:3000/agents/:agentId

    h. *To get all tasks*

    GET http://localhost:3000/tasks

    i. *To retrieve single task with taskId as param*

    GET http://localhost:3000/tasks/:taskId

    j. *To delete agent with taskId*

    DELETE http://localhost:3000/tasks/:taskId

# Known errors:

If there are no avilable agents to work on task, application throws an error and also create a agent record in Agent collection with null values. This is due to upsert: true set in the findOneAndUpdate method.
