const Agent = require("../models/agent.model.js");
const Task = require("../models/tasks.model.js");
const { validationResult } = require("express-validator");

// Create and Save a new Agent

exports.create = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  // Create a Agent
  const agent = new Agent({
    agentName: req.body.agentName,
    agentSkills: req.body.agentSkills,
    taskAssigned: false
  });

  // Save Agent in the database
  agent
    .save()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating Agent details."
      });
    });
};

// Retrieve and return all agents from the database.
exports.findAll = (req, res) => {
  Agent.find()
    .then(agentData => {
      res.send(agentData);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving Agent collection.."
      });
    });
};

// Find a single agent with an agentId
exports.findOne = (req, res) => {
  Agent.findById(req.params.agentId)
    .then(agentData => {
      if (!agentData) {
        return res.status(404).send({
          message: "Agent not found with id " + req.params.agentId
        });
      }
      res.send(agentData);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Agent not found with id " + req.params.agentId
        });
      }
      return res.status(500).send({
        message: "Error retrieving Agent with id " + req.params.agentId
      });
    });
};

// Update an agent name identified by the agentId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.agentName) {
    return res.status(400).send({
      message: "Agent name can not be empty"
    });
  }

  // Find Agent and update it with the request body
  Agent.findByIdAndUpdate(
    req.params.agentId,
    {
      agentName: req.body.agentName
    },
    { new: true }
  )
    .then(agentData => {
      if (!agentData) {
        return res.status(404).send({
          message: "Agent not found with id " + req.params.agentId
        });
      }
      res.send(agentData);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Agent not found with id " + req.params.agentId
        });
      }
      return res.status(500).send({
        message: "Error updating Agent name with id " + req.params.agentId
      });
    });
};

// Update an agent name identified by the agentId in the request
exports.updateSkills = (req, res) => {
  // Validate Request
  if (!req.body.agentSkills) {
    return res.status(400).send({
      message: "Agent Skills can not be empty"
    });
  }

  // Find Agent and update it with the request body
  Agent.findByIdAndUpdate(
    req.params.agentId,
    {
      agentSkills: req.body.agentSkills
    },
    { new: true }
  )
    .then(agentData => {
      if (!agentData) {
        return res.status(404).send({
          message: "Agent not found with id " + req.params.agentId
        });
      }
      res.send(agentData);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Agent not found with id " + req.params.agentId
        });
      }
      return res.status(500).send({
        message: "Error updating Agent skills with id " + req.params.agentId
      });
    });
};

// Delete an Agent with the specified agentId in the request
exports.delete = (req, res) => {
  Agent.findByIdAndRemove(req.params.agentId)
    .then(agentData => {
      if (!agentData) {
        return res.status(404).send({
          message: "Agent not found with Name " + req.params.agentId
        });
      }
      res.send({ message: "Agent deleted successfully!" });
    })
    .catch(err => {
      return res.status(500).send({
        message: "Could not delete Agent with name " + req.params.agentId
      });
    });
};
