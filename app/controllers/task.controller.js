const Task = require("../models/tasks.model.js");
const Agent = require("../models/agent.model.js");
const { validationResult } = require("express-validator");

// Create and Save a new Task
exports.create = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  // Find matching agent and assign the task
  findMatchingAgent();

  function findMatchingAgent() {
    Agent.findOneAndUpdate(
      {
        $or: [
          {
            agentSkills: { $in: req.body.skills },
            taskAssigned: false
          },
          {
            assignedTaskPriority: "low",
            taskAssigned: true,
            agentSkills: { $in: req.body.skills }
          }
        ]
      },
      { $set: { taskAssigned: true, assignedTaskPriority: req.body.priority } },
      { new: true, upsert: true, strict: false },
      (err, agent) => {
        if (err) {
          console.log("There are no matching agents");
          console.log(err);
        }
        var matchedAgent = agent.agentName;
        if (!matchedAgent) {
          deleteUpsert();
          res.status(500).json({
            message: " There are no matching agents. Task cannot be created"
          });
        } else {
          updateTask(matchedAgent);
        }
      }
    ).sort({ updatedAt: -1 });
  }

  function deleteUpsert() {
    Agent.findOneAndDelete({}).sort({ updatedAt: -1 });
    console.log("In delete");
  }

  // Create a new task by assigning a matched agent
  function updateTask(matchedAgent) {
    const task = new Task({
      agentAssigned: matchedAgent,
      taskName: req.body.taskName,
      skills: req.body.skills,
      priority: req.body.priority,
      taskStatus: "Active"
    });

    // Save task in the database
    task
      .save()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating Task details."
        });
      });
  }
};

// Retrieve and return all tasks from the database.
exports.findAll = (req, res) => {
  Task.find()
    .then(taskData => {
      res.send(taskData);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving Task collection.."
      });
    });
};

// Find a single task with a taskId
exports.findOne = (req, res) => {
  Task.findById(req.params.taskId)
    .then(Data => {
      if (!Data) {
        return res.status(404).send({
          message: "task not found with id " + req.params.taskId
        });
      }
      res.send(Data);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Task not found with id " + req.params.taskId
        });
      }
      return res.status(500).send({
        message: "Error retrieving task with id " + req.params.taskId
      });
    });
};

// Update a Task identified by the taskId in the request
exports.update = (req, res) => {
  completeTask();

  // Unassign agent once task is completed
  function freeAgent(agentAssigned) {
    Agent.findOneAndUpdate(
      { agentName: agentAssigned },
      { $set: { taskAssigned: false } },
      { new: true },
      (err, agent) => {
        if (err) {
          console.log("There are no matching agents");
          console.log(err);
          console.log(skills);
        }
        console.log("Successuly freed the agent ");
      }
    );
  }

  // Complete task and free the agent
  function completeTask() {
    Task.findOneAndUpdate(
      { taskName: req.params.taskName },
      { $set: { taskStatus: "Complete" } },
      { new: true },
      (err, task) => {
        if (err) {
          console.log("There are no matching tasks");
          console.log(err);
          console.log(req.params.taskName);
        }
        console.log("task.agentAssigned", task.agentAssigned);
        freeAgent(task.agentAssigned);
        res.send(task);
      }
    );
  }
};

// Delete a task with the specified taskId in the request
exports.delete = (req, res) => {
  Task.findByIdAndRemove(req.params.taskId)
    .then(agentData => {
      if (!agentData) {
        return res.status(404).send({
          message: "Task not found with Id " + req.params.taskId
        });
      }
      res.send({ message: "task deleted successfully!" });
    })
    .catch(err => {
      return res.status(500).send({
        message: "Could not delete Task with Id " + req.params.taskId
      });
    });
};
