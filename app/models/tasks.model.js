const mongoose = require("mongoose");

const TaskSchema = mongoose.Schema(
  {
    agentAssigned: {
      type: String
    },
    taskName: {
      type: String,
      required: true,
      unique: true
    },
    skills: {
      type: [String],
      required: true
    },
    priority: {
      type: String,
      enum: ["High", "Low", "low", "high"]
    },
    taskStatus: {
      type: String,
      enum: ["Active", "Complete", "Suspend"]
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Task", TaskSchema);
