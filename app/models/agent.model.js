const mongoose = require("mongoose");

const AgentSchema = mongoose.Schema(
  {
    agentName: {
      type: String,
      required: true,
      unique: true,
      sparse: true
    },
    agentSkills: {
      type: [String],
      required: true
    },
    assignedTaskPriority: {
      type: String,
      enum: ["High", "Low", "low", "high"]
    },
    taskAssigned: {
      type: Boolean
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Agent", AgentSchema);
