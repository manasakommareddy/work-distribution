module.exports = app => {
  const tasks = require("../controllers/task.controller.js");
  const { check, validationResult } = require("express-validator");

  // Create a new Task
  app.post(
    "/create-task",
    [
      check(
        `skills`,
        `Skills must be passed as an array. Ex: skills[0]: skills1, skills[1]: skills2 `
      ).isArray(),
      check("priority", "Priority can only be High or Low")
        .not()
        .isEmpty()
    ],
    tasks.create
  );

  // Retrieve all Tasks
  app.get("/tasks", tasks.findAll);

  // Retrieve a single Task with taskName
  app.get("/tasks/:taskId", tasks.findOne);

  // Update a Task with taskName
  app.put("/complete-task/:taskName", tasks.update);

  // Delete a task with taskId
  app.delete("/tasks/:taskId", tasks.delete);
};
