module.exports = app => {
  const agents = require("../controllers/agent.controller.js");
  const { check, validationResult } = require("express-validator");

  // Create a new Agent
  app.post(
    "/create-agent",
    [
      [
        check("agentName", "Agent name is required")
          .not()
          .isEmpty(),
        check(
          "agentSkills",
          `Agent Skills must be passed as an array. Ex: agentSkills[0]: skills1, agentSkills[1]: skills2`
        ).isArray()
      ]
    ],
    agents.create
  );

  // Retrieve all agents
  app.get("/agents", agents.findAll);

  // Retrieve a single Agent with agentName
  app.get("/agents/:agentId", agents.findOne);

  // Update an Agent name with agentId
  app.put("/agents/agent-name/:agentId", agents.update);

  // Update an Agent skills with agentId
  app.put("/agents/agent-skills/:agentId", agents.updateSkills);

  // Delete an Agent with agentId
  app.delete("/agents/:agentId", agents.delete);
};
